﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PropertyCompany.Helper;
using PropertyCompany.Models;

namespace PropertyCompany.DAL
{
    public class دفتر_مصروفات_نثريه_DAL
    {
        public static int AddNewData(OperatingExpensesBook nwBook)
        {
            try
            {
                using (PropertyCompanyEntities dbContext = new PropertyCompanyEntities())
                {
                    dbContext.OperatingExpensesBooks.Add(nwBook);
                    if (dbContext.SaveChanges() > 0)
                        return 1;

                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}