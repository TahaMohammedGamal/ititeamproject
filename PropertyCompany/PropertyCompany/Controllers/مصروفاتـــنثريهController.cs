﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PropertyCompany.Helper;
using PropertyCompany.Models;

namespace PropertyCompany.Controllers
{
    public class مصروفاتـــنثريهController : Controller
    {
        // GET: مصروفاتـــنثريه
        public ActionResult Index()
        {
            InBagClient();
            return View();
        }

        private void InBagClient()
        {
            ClientBag bag = new ClientBag();

            bag += new ServerParam("OpertObj", new OperatingExpensesBook());

            ViewBag.ServerParams = bag;
        }
    }
}