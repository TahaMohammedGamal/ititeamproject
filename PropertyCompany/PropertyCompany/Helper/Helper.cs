﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace PropertyCompany.Helper
{
    public class Helper
    {
        public static string ToJsonNS(object obj, bool handleRefLoop = true)
        {
            try
            {
                if (handleRefLoop)
                    return JsonConvert.SerializeObject(obj, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                else
                    return JsonConvert.SerializeObject(obj);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string Status { set; get; }
        public string ErrorMessage { set; get; }
        public bool IsError { set; get; }
        public Dictionary<string, object> ServerParams { set; get; }

    }

    public class ServerParam
    {
        public string Key { set; get; }
        public object Value { set; get; }

        public ServerParam()
        {

        }

        public ServerParam(string key, object value)
        {
            Key = key;
            Value = value;
        }

    }

    public class ClientBag : Dictionary<string, object>
    {
        public ClientBag()
        {

        }
        public ClientBag(string key, object val)
        {
            Add(key, val);
        }

        public ClientBag(params ServerParam[] parameters)
        {
            foreach (ServerParam parameter in parameters)
                Add(parameter.Key, parameter.Value);
        }

        public static ClientBag operator +(ClientBag obj, ServerParam parameter)
        {
            obj.Add(parameter.Key, parameter.Value);
            return obj;
        }

        public static ClientBag operator -(ClientBag obj, string key)
        {
            obj.Remove(key);
            return obj;
        }



    }



}